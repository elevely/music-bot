import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
import json
import pickle
from collections import Counter

def wordcloud(corpus):
    with open("./data/config.json", 'r') as file_1:
        config = json.load(file_1)    
        with open(config['path_to_stopwords'], 'rb') as file_2: # 10-15 строки - исключение мусорных слов
            stopwords = pickle.load(file_2)
            for word in corpus:
                if word.lower() in stopwords or len(word) == 1:
                    corpus[word] = 0
            corpus += Counter()
            word_hl = WordCloud(background_color='white',
                            colormap="Reds",
                            stopwords=stopwords, #не робит, поэтому сделал ручками
                            margin=2,
                            font_path=config['path_to_font'],
                            mode='RGB',
                            width=1920,
                            height=1080)
            img_cloud = word_hl.generate_from_frequencies(corpus)
            img_cloud.to_file(config['path_to_wordcloud'])
