# by Vladimir Goncharov aka elevely, 2018

from collections import defaultdict, Counter
import re
import pickle
import nltk
import math
import statistics


class Lyrics:

    def __init__(self, text="", title="", href="", tokenized_text=""):
        self.text = text
        self.title = title
        self.href = href
        self.keywords = set()
        self.tokenized_text = tokenized_text

    def add_keyword(self, keyword):
        self.keywords.add(keyword)


class LyricsCollection:
    def __init__(self):
        self.songs = defaultdict(dict)
        self.counter = 1
        self.word_collection = Counter()

    def normalize(self, text):
        tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
        clear_text = tokenizer.tokenize(text)
        counter_for_clear_text = Counter()
        wordnet_lemmatizer = nltk.WordNetLemmatizer()
        for word in clear_text:
            counter_for_clear_text[wordnet_lemmatizer.lemmatize(word)] += 1
        return counter_for_clear_text


    def find_by_keywords(self, keywords):
        answer = set()
        for artist_songs in self.songs:
            for song in self.songs[artist_songs]: 
                if keywords <= song.keywords:
                    answer.add(song)
        return answer

    def add_song(self, text, title, artist, href):

        self.counter += 1
        tokenized_text = self.normalize(text)
        self.word_collection += tokenized_text
        self.songs[artist][self.counter] = Lyrics(text, title, href, tokenized_text)

    def compute_tfidf(self):
        def compute_tf(text):
            for i in text:
                text[i] = text[i]/float(len(text))
            return text

        def compute_idf(word, songs):
            return math.log10(sum(songs.values()) / songs[word])
     
        tf_idf_dictionary = {}
        for artist_songs in self.songs:
            for song_id in self.songs[artist_songs]:
                
                tf_idf_dictionary[artist_songs] = {}
                tf_idf_dictionary[artist_songs][song_id] = {}
                computed_tf = compute_tf(self.songs[artist_songs][song_id].tokenized_text)
                for word in computed_tf:
                    tf_idf_dictionary[artist_songs][song_id][word] = computed_tf[word] * compute_idf(word, self.word_collection)
        return tf_idf_dictionary

    def print_keyword(self):
        for i in self.songs:
            for j in self.songs[i]:
                print(self.songs[i][j].keywords)
            print('---------------------------')

    def find_keywords(self):
        frequencies = self.compute_tfidf()
        for artist_songs in frequencies:
            for song in frequencies[artist_songs]:
                for word in frequencies[artist_songs][song]:
                    if frequencies[artist_songs][song][word] > 1.2 * statistics.mean(frequencies[artist_songs][song].values()) and (len(word) > 1 or word == 'i'):
                        self.songs[artist_songs][song].add_keyword(word)

def load_model(path):
    with open(path, 'rb') as model:
        base = pickle.load(model)
        return base

def save_model(base, path):
    with open(path, "wb") as model:
            pickle.dump(base, model, 2)