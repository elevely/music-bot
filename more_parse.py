# by Vladimir Goncharov aka elevely, 2018

from bs4 import BeautifulSoup
import requests
import json
import re
from lyprics import *
import pickle
import os
import math

def find_pages_with_artists_lists(url):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    pages_with_artist_links = set()
    for letter in alphabet:
        response = requests.get(url + '/' + letter + "/").text
        soup = BeautifulSoup(response, 'lxml')
        links = soup.find_all('li', class_='li_pagination')
        for i in links:
            page_number = str(i.contents[0])[9:].split('"')
            if page_number[0] not in pages_with_artist_links:
                pages_with_artist_links.add(page_number[0])
    return pages_with_artist_links


def find_artist_links(url, pages_with_artists_lists, path_to_data, path_to_artists_links):
    assert (os.path.exists(path_to_data) and os.path.isdir(path_to_data)), "Invalid path for pages with artists links"
    with open(path_to_artists_links,  mode='w', encoding='UTF-8') as file:
        data_artist_links = set()
        for page_with_artists_link in pages_with_artists_lists:
            try:
                response = requests.get(url + page_with_artists_link).text
                soup = BeautifulSoup(response, 'lxml')
                tag = soup.find('table', class_='tracklist')
                artist_links = tag.find_all('a')
                for i in artist_links:
                    file.write(str(i.get('href')) + '\n')
            except:
                print("Invalid link: {}".format(str(tag)))


def find_tracks_links(path_to_artists_links, path_to_track_links):
    assert (os.path.exists(path_to_artists_links) and os.path.isfile(path_to_artists_links)), "Invalid path to artists links"
    assert (os.path.exists(path_to_track_links) and os.path.isfile(path_to_track_links)), "Invalid path to tracks links"
    with open(path_to_artists_links, mode='r', encoding='UTF-8') as file:
        with open(path_to_track_links, mode='a', encoding='UTF-8') as tracks:
            for artist_link in file:
                try:
                    response = requests.get(artist_link).text
                    soup = BeautifulSoup(response, "lxml")
                    tag = soup.find('table', class_='tracklist')
                    track_links = tag.find_all('a')
                    for i in track_links:
                        tracks.write(str(i.get('href')) + "\n")
                except:
                    print("Invalid link: {}".format(str(tag)))


def find_text_and_title_of_tracks(path_to_track_links, path_to_model):
    assert (os.path.exists(path_to_track_links) and os.path.isfile(path_to_track_links)), "Invalid path to track links"
    base = LyricsCollection()
    with open(path_to_track_links, mode='r', encoding='UTF-8') as file:
        counter = 1
        bad_counter = 1
        for track_link in file:
            try:
                response = requests.get(track_link).text
                soup = BeautifulSoup(response, 'lxml')
                concrete_text = soup.find(
                    'p', class_='songLyricsV14 iComment-text').text
                caption = soup.find(
                    'div', class_='pagetitle').contents[1].text[:-6]
                artist, title = caption.split(" - ") 
                print(title)
                base.add_song(concrete_text, title, artist, track_link)
                print(counter)
                counter += 1
            except KeyboardInterrupt:
                break
            except:
                bad_counter += 1
                print("Invalid link: {}".format(bad_counter))
                print(track_link)
                

    with open(path_to_model, "wb") as model:
        pickle.dump(base, model, 2)


def remove_duplicate(path_to_track_links):
    assert (os.path.exists(path_to_track_links) and os.path.isfile(path_to_track_links)), "Invalid path to track links"
    lines = set()
    with open(path_to_track_links, 'r') as tracks:
        for i in tracks:
            lines.add(i)

    with open(path_to_track_links, 'w') as tracks:
        for line in lines:
            tracks.write(line)


def main():
    url = 'http://www.songlyrics.com'
    with open("./data/config.json", 'r') as file:
        config = json.load(file)
        pages_with_artists_lists = find_pages_with_artists_lists(url)
        # find_artist_links(url, pages_with_artists_lists, config["path_to_data"], config["path_to_artists_links"])
        # find_tracks_links(config['path_to_artists_links'], config['path_to_track_links'])
        # remove_duplicate(config['path_to_track_links'])
        find_text_and_title_of_tracks(config['path_to_track_links'], config['path_to_model_without_keywords'])
        base = load_model(config['path_to_model_without_keywords'])
        base.find_keywords()
        save_model(base, config['path_to_model'])


if __name__ == '__main__':
    main()
