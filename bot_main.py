# by Vladimir Goncharov aka elevely, 2018

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
from collections import defaultdict, Counter
import re
import json
import pickle
from lyprics import *
import random
from enum import Enum
import os
from word_cloud import wordcloud

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


class Bot:
    def __init__(self, base):
        with open("./data/config.json", 'r') as file:
            config = json.load(file)
        self.token = config['token']
        self.url = "https://api.telegram.org/bot{}/".format(self.token)
        self.updater = Updater(self.token)
        self.dp = self.updater.dispatcher
        handlers = {CommandHandler("start", self.start), 
                    CommandHandler("help", self.help),
                    CommandHandler("keyword", self.keyword), 
                    CommandHandler("text", self.text),
                    CommandHandler("artist_wordcloud", self.artist_wordcloud),
                    CommandHandler("track_wordcloud", self.track_wordcloud), 
                    MessageHandler(Filters.text, self.cur_f)}
        for handler in handlers:
            self.dp.add_handler(handler)
        self.dp.add_error_handler(self.error)
        self.base = base
        self.states = Enum('states', 'nothing keyword text artist_wordcloud track_wordcloud')
        self.cur_state = self.states.nothing


    def start(self, bot, update):
        bot.send_message(chat_id=update.message.chat_id,
                         text='Hi! I am P E R E S D A C H A bot')


    def help(self, bot, update):
        with open("./data/config.json", 'r') as file:
            config = json.load(file)
            if os.path.exists(config['path_to_help']) and os.path.isfile(config['path_to_help']):
                f = open(config['path_to_help'], "r")
                for line in f:
                    bot.send_message(chat_id=update.message.chat_id, text=line)
            else:
                print("Config error: invalid path to file with help info")
                bot.send_message(chat_id=message.chat_id, text="Command not available")


    def keyword(self, bot, update):
        bot.send_message(chat_id=update.message.chat_id,
                         text="Write keywords with spaces")
        self.cur_state = self.states.keyword


    def text(self, bot, update):
        bot.send_message(chat_id=update.message.chat_id,
                         text="Write the part of text")
        self.cur_state = self.states.text

    def artist_wordcloud(self, bot, update):

        bot.send_message(chat_id=update.message.chat_id,
                         text="Write the artist")
        self.cur_state = self.states.artist_wordcloud

    def track_wordcloud(self, bot, update):
        bot.send_message(chat_id=update.message.chat_id,
                         text="Write the artist and the title of the song through \" - \"")
        self.cur_state = self.states.track_wordcloud

    def find_by_keyword(self, bot, message):
        keywords = set(message.text.split())
        songs = self.base.songs
        found = False
        for artist_songs in songs:
            for i in songs[artist_songs]:
                song = songs[artist_songs][i]
                if keywords.issubset(song.keywords):
                    found = True
                    bot.send_message(chat_id=message.chat_id, 
                                     text=(song.title + " - " + artist_songs + "\n" + song.href + '\n' + song.text))
        if found == False:
            self.send_random_song(bot, message.chat_id, "Nothing found")


    def find_by_text(self, bot, message):
        found = False
        songs = self.base.songs

        for artist_songs in songs:
            for i in songs[artist_songs]:
                song = songs[artist_songs][i]
                if message.text.lower() in song.text.lower():
                    print(song)
                    found = True
                    bot.send_message(chat_id=message.chat_id, text=(song.title + " - " + artist_songs + "\n" +
                                                                    song.href + '\n' + song.text))
        if found == False:
            self.send_random_song(bot, message.chat_id, "Nothing found")


    def send_random_song(self, bot, chat_id, error_messsage):
        track = random.choice(list(self.base.songs.items()))
        artist = track[0]
        songs = track[1]
        song = random.choice(list(songs.items()))[1]
        bot.send_message(chat_id=chat_id, text=(error_messsage + "\n" + song.title + " - " + artist + "\n" +
                                                song.href + '\n' + song.text))


    def error(self, bot, update, error):
        logger.warning('Update "%s" caused error "%s"', update, error)

    def describe_artist(self, bot, message):
        artist = message.text
        corpus = Counter()
        for song in self.base.songs[artist]:
            corpus += self.base.songs[artist][song].tokenized_text
        if len(corpus) == 0:
            bot.send_message(chat_id=message.chat_id, text=("Artist not found"))
        else:
            with open("./data/config.json", 'r') as file:
                config = json.load(file)
                wordcloud(corpus)    
                bot.sendPhoto(chat_id=message.chat_id, photo=open(config['path_to_wordcloud'], 'rb'))


    def describe_track(self, bot, message):

        try:
            artist, track = message.text.split(" - ")
            songs = self.base.songs[artist]
            for song_id in songs:
                if track.lower() in songs[song_id].title.lower():
                    corpus = songs[song_id].tokenized_text
                    break
            if len(corpus) == 0:
                bot.send_message(chat_id=message.chat_id, text=("Track not found"))
            else:
                with open("./data/config.json", 'r') as file:
                    config = json.load(file)
                    wordcloud(corpus)    
                    bot.sendPhoto(chat_id=message.chat_id, photo=open(config['path_to_wordcloud'], 'rb'))
        except:
            bot.send_message(chat_id=message.chat_id, text=("Invalid input"))



    def cur_f(self, bot, update):
        if self.cur_state == self.states.nothing:
            bot.send_message(chat_id=update.message.chat_id,
                             text="Choose a command")
        elif self.cur_state == self.states.keyword:
            self.find_by_keyword(bot, update.message)
        elif self.cur_state == self.states.text:
            self.find_by_text(bot, update.message)
        elif self.cur_state == self.states.artist_wordcloud:
            self.describe_artist(bot, update.message)
        elif self.cur_state == self.states.track_wordcloud:
            self.describe_track(bot, update.message)
        else:
            self.send_random_song(
                bot, update.message.chat_id, "Invalid command")


def main():
    with open("./data/config.json", 'r') as file:
        config = json.load(file)
        assert(os.path.exists(config['path_to_model']) and os.path.isfile(config['path_to_model'])), \
                              "Config error: invalid path to file with model"
        with open(config['path_to_model'], 'rb') as model:
            base = pickle.load(model)
            BotParser = Bot(base)
            BotParser.updater.start_polling()
            BotParser.updater.idle()


if __name__ == '__main__':
    main()
